<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('index-1');
	}

	public function dev()
	{
		return EasyREST($this, [
			// "UPLOAD" => function($params) {
			// 	EasyUpload($params['files']['avatar'], __DIR__);
			// 	return $params;
			// },

			"TAMBAH_USER" => function($params) {
				$sudahMasuk = $this->db->simple_query("INSERT INTO `user` (`id_pengguna`, `nama_pengguna`, `username`, `password`)
				VALUES ('8', '" . $params["request"]["nama"] .  "', 'ulum', 'ulum')");
				return $sudahMasuk;
			},



			"GET_USER" => function($params) {
				if (isset($params["request"]["user_id"])) {
					$userId = $params["request"]["user_id"];
					$users = $this->db->query("select * from profil, teman where profil.id_user = teman.id_teman and day(profil.tanggal_lahir_user)=day(curdate()) and month(profil.tanggal_lahir_user)= month(curdate()) and teman.id_user !=" . $userId)->result();
				} else {
					$users = $this->db->query("select * from profil")->result();
				}
				return $users;
			},

			"VIEW" => function($params) {
				$this->load->view('index-1');
			}
		]);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tugas_b extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->view('index-1');
	}


	public function block()
	{
		$this->load->view('block');
	}
	public function block_success()
	{
		$this->load->view('block_success');
	}
	public function index_unblock()
	{
		$this->load->view('index_unblock');
	}


	public function select()
	{
		$this->db->SELECT('*')
				 ->FROM('Block_friend');
		$queri	=$this->db->get();
		$row	=$queri->row();

		$kolom = array(
			'ID_user' => $row->ID_user,
			'user'	=>	$row->user,
			'user_block' =>	$row->user_block,
			'Tanggal'	=>	$row->Tanggal
		);
		return $kolom;
	}
}

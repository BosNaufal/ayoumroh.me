
<?php
/*!
 * Copyright (c) 2016 Naufal Rabbani (http://github.com/BosNaufal),
 * Licensed Under MIT (http://opensource.org/licenses/MIT),
 * 
 * EasyREST,
 * 
 */

function EasyUpload($file, $dest, $beforeMoved = null) {
  $tmp_name = $file['tmp_name'];
  $name = basename($file['name']);
  if (isset($beforeMoved)) {
    $name = $beforeMoved($file);
  }
  move_uploaded_file($tmp_name, $dest . '/' . $name);
}

function EasyREST($context, $handlers) {
  $context->load->helper('url');
  $context->load->database();
  $is_post = $context->input->method() == 'post';
  $params = (array) json_decode(file_get_contents('php://input'));
  $reqType = $context->input->get_request_header('Content-Type');
  $isMultipart = strpos($reqType, "multipart/form-data;") !== false;
  if ($isMultipart) {
    $params = $_POST;
  }
  $allParameter = [
    "request" => $params,
    "query" => $_GET,
    "files" => $_FILES 
  ];
  if ($is_post) {
    header('Content-Type: application/json');
    echo json_encode($handlers[$params["type"]]($allParameter));
  } else {
    $handlers["VIEW"]($allParameter);
  }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Teman Dekat</title>

    <link href="<?= base_url('/assets/normalize.css') ?>">

    <style>
      * {
        box-sizing: border-box;
      }

      body {
        font-family: sans-serif;
        font-size: 0.9em;
        background: #e5e5e5;
      }

      button {
        cursor: pointer;
        outline: 0;
      }

      .teman-dekat {
        transition: all 0.5s ease;
        background: #008eff;
        padding: 7px 15px;
        color: white;
        border-radius: 3px;
        border: 0;
        font-size: 0.85em;
        cursor: pointer;
      }

      .teman-dekat:hover {
        background: #006ec5;
      }

      .teman-dekat.batal {
        background: grey;
        color: #2b2b2b;
        background: #e6e6e6;
      }

      .teman-dekat.tes-error {
        background: red;
      }

      .modal-outer {
        background: rgba(0,0,0,.7);
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }

      .modal-inner {
        background: white;
        width: 450px;
        margin: 5% auto;
        border-radius: 5px;
        overflow: hidden;
      }

      .modal-header, .modal-body {
        padding: 15px;
        color: #393939;
      }

      .modal-header {
        text-align: center;
        padding: 10px 5px;
        background: #008eff;
        color: white;
      }

      .modal-error .modal-header {
        background: red;
      }

      .teman-dekat-wrapper {
        padding: 15px;
      }

      .teman-dekat-wrapper h1 {
        font-size: 1.2em;
        margin-top: 0;
      }

      .base-wrapper {
        background: white;
        box-shadow: 2px 1px 3px 0 rgba(0,0,0,.2);
        border-radius: 3px;
        overflow: hidden;
      }
      
      .notification-wrapper {
        width: 350px;
        margin-left: 17px;
      }

      .notification-header {
        padding: 10px 15px;
        background: #008eff;
        color: white;
        font-weight: bold;
        text-align: center;
      }

      .notification-list {
        max-height: 230px;
        overflow: auto;
      }

      .notification-item {
        cursor: pointer;
        display: flex;
        padding: 10px 15px;
        border-bottom: 1px solid #DDD;
      }

      .notification-item:hover {
        background: #f6feff;
      }

      .notification-icon {
        width: 45px;
        height: 45px;
        background: #DDD;
        margin-right: 10px;
      }

      .notification-body {
        padding: 3px 0;
        flex: 1;
      }

      .notification-description {
        color: #2b2b2b;
        font-size: 1em;
        margin-bottom: 3px;
      }
      .notification-date {
        color: #888;
      }

      .flex-container {
        display: flex;
      }

      .flex-container.column {
        flex-direction: column;
      }

      .left-column {
        width: 350px;
      }

      .status-wrapper {
        padding: 10px 15px;
        margin-top: 15px;
      }

      .status-inner {
        position: relative;
        width: 100%;
      }

      .status-header {
        font-weight: bold;
        margin: 7px 0;
      }

      .status-input {
        height: 75px;
        border: 1px solid #DDD;
        width: 100%;
        outline: 0;
        padding: 5px 10px;
        font-size: 1em;
      }

      .status-actions {
        margin-top: 7px;
        text-align: right;
      }
      
      .status-actions button {
        padding: 5px 15px;
        font-size: 1em;
        background: #008eff;
        color: white;
        border-radius: 3px;
        outline: 0;
        border: 0;
      }

      .main-container {
        display: block;
        margin: 0 auto;
      }

      .keterampilan-wrapper {
        margin-top: 20px;
        padding: 15px;
      }

      .keterampilan-header {
        border-bottom: 1px solid #DDD;
        color: #888;
        font-weight: bold;
        padding-bottom: 7px;
        margin-bottom: 10px;
      }

      .keterampilan-list a {
        font-weight: bold;
        color: black;
        text-decoration: none;
        display: inline-block;
        margin-bottom: 5px;
      }

      .keterampilan-list a span {
        margin-right: 7px;
      }

      .keterampilan-list a.edit {
        color: #008eff;
        margin-left: 15px;
      }

      .keterampilan-list a:hover {
        text-decoration: underline;
      }

      .keterampilan-input {
        margin-bottom: 15px;
        margin-top: 10px;
      }

      .keterampilan-footer {
        border-top: 1px solid #DDD;
        padding-top: 10px;
        text-align: right;
      }

      .keterampilan-footer select {
        margin-right: 15px;
      }
      .keterampilan-footer button {
        padding: 5px 10px;
        border-radius: 3px;
        border: 0;
        outline: 0;
        margin-right: 3px;
        font-size: 0.8em;
      }
      .keterampilan-footer button.simpan {
        background: #008eff;
        color: white;
      }
      .keterampilan-footer button.batal {
        background: #DDD;
      }
      

    </style>
  </head>
  <body>
    <div id="app-keterampilan">
      <div class="main-container">
        <div class="flex-container">
          <div class="flex-container column left-column">
            <div class="teman-dekat-wrapper base-wrapper">
              <h1>Teman Dekat</h1>
              <button class="teman-dekat" :class="{ batal: hasCloseFriend }" @click="updateTemanDekat">
                {{
                  !hasCloseFriend?
                    "Tambahkan ke teman dekat"
                  :
                    "Batalkan teman dekat"
                }}
              </button>
              <button class="teman-dekat tes-error" @click="openModalError">Tes error</button>
            </div>
            <!-- <div class="base-wrapper status-wrapper">
              <div class="status-inner">
                <div class="status-header">Status Saya</div>
                <textarea class="status-input" v-model="statusInput"></textarea>
                <div class="status-actions">
                  <button @click="addStatus">Kirim</button>
                </div>
              </div>
            </div> -->
          </div>
          
          <!-- <div class="base-wrapper notification-wrapper">
            <div class="notification-header">Notifikasi</div>
            <div class="notification-list">
              <div class="notification-item" v-for="notif in notifications">
                <div class="notification-icon"></div>
                <div class="notification-body">
                  <div class="notification-description">{{ notif }}</div>
                  <div class="notification-date">Just Now</div>
                </div>
              </div>
            </div>
          </div> -->
        </div>

        <div>
          <div class="base-wrapper keterampilan-wrapper">
            <div class="keterampilan-header">KETERAMPILAN PROFESIONAL</div>
            <div class="keterampilan-body">
              <div class="keterampilan-list" v-if="!keterampilanActive">
                <a href="#" v-for="(keterampilan, index) in keterampilanList">{{ keterampilan }} <span v-if="index !== keterampilanList.length - 1">-</span></a>
                <a href="#" class="edit" @click="setKeterampilanActive(true)">Edit</a>
              </div>
              <div v-if="keterampilanActive">
                <div class="keterampilan-input">
                  <input-tag :tags.sync="keterampilanTagsModel"></input-tag>
                </div>
                <div class="keterampilan-footer">
                  <!-- <select>
                    <option>Publik</option>
                    <option>Privat</option>
                  </select> -->
                  <button class="simpan" @click="keterampilanSimpan">Simpan</button>
                  <button class="batal" @click="keterampilanBatal">Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      

      <div class="modal-outer" @click="closeModal" v-if="showModal">
        <div class="modal-inner">
          <div class="modal-header">Berhasil</div>
          <div class="modal-body">
            Anda telah {{ !hasCloseFriend? 'membatalkan' : 'menambahkan' }} teman sebagai teman dekat
          </div>
        </div>
      </div>

      <div class="modal-outer modal-error" @click="closeModalError" v-if="showModalError">
        <div class="modal-inner">
          <div class="modal-header">Error</div>
          <div class="modal-body">
            Terjadi kesalahan pada server saat memproses permintaan anda
          </div>
        </div>
      </div>

    

    </div>

    <script src="<?= base_url('/assets/teman-dekat/assets/vue.js') ?>"></script>
    <script src="<?= base_url('/assets/teman-dekat/assets/vue-input-tag.min.js') ?>"></script>
    <script src="<?= base_url('/assets/teman-dekat/assets/axios.min.js') ?>"></script>
    <?php 
      echo '<script> var userId = ' . $_GET['user'] . ' </script>';
      if (isset($_GET['teman'])) {
        echo '<script> var temanId = ' . $_GET['teman'] . ' </script>';
      }
    ?>
    <script>
      Vue.component('input-tag', InputTag);

      new Vue({
        el: "#app-keterampilan",
        data() {
          return {
            showModal: false,
            showModalError: false,
            hasCloseFriend: false,
            
            statusInput: "",

            notifications: [],

            keterampilanTagsModel: [],
            keterampilanList: [],
            keterampilanActive: false,
          }
        },
        
        created() {
          this.getStatusTemanDekat()
          this.getKeterampilan()
        },

        methods: {
          getKeterampilan () {
            axios.get('index.php/data/get', { 
              params: {
                query: "select ketrampilan_user from profil where id_user = '" + userId + "';"
              }
            }).then((res) => {
              this.keterampilanList = res.data[0].ketrampilan_user.split(',')
            })
          },
          getStatusTemanDekat () {
            axios.get('index.php/data/get', { 
              params: {
                query: "select * from teman_dekat where id_user = '" + userId + "' and id_teman = '" + temanId + "';"
              }
            }).then((res) => {
              if (res.data.length) {
                this.hasCloseFriend = res.data[0].id
              } 
            })
          },
          updateTemanDekat () {
            let query;
            if (this.hasCloseFriend) {
              query = "DELETE FROM teman_dekat WHERE id = '" + this.hasCloseFriend + "';"
            } else {
              query = "INSERT INTO teman_dekat (id_user, id_teman) VALUES ('" + userId + "', '" + temanId + "');"
            }
            return axios.get('index.php/data/mutation', { 
              params: {
                query: query,
              }
            }).then((res) => {
              this.hasCloseFriend = !this.hasCloseFriend
              this.openModal()
            })
          },
          openModal() {
            this.showModal = true
          },
          closeModal() {
            this.showModal = false
          },
          openModalError() {
            this.showModalError = true
          },
          closeModalError() {
            this.showModalError = false
          },
          addStatus() {
            this.statusInput = ""
            if (this.hasCloseFriend) {
              this.notifications.push("Teman dekat anda menambahkan status baru")
            }
          },
          clearNotif(index) {
            this.notifications.splice(index, 1)
          },
          setKeterampilanActive(state) {
            if (state) {
              this.keterampilanTagsModel = this.keterampilanList
            }
            this.keterampilanActive = state
          },
          updateKeterampilan () {
            axios.get('index.php/data/mutation', { 
              params: {
                query: "UPDATE profil SET ketrampilan_user = '" + this.keterampilanList.join(',') + "' where id_user = '" + userId + "';"
              }
            }).then((res) => {
              if (res.data) alert('berhasil')
            })
          },
          keterampilanSimpan() {
            const tags = this.keterampilanTagsModel
            this.keterampilanList = tags
            this.keterampilanTagsModel = []
            this.setKeterampilanActive(false)
            this.updateKeterampilan()
          },
          keterampilanBatal() {
            this.keterampilanTagsModel = []
            this.setKeterampilanActive(false)
          },
        }
      })      
    </script>
  </body>
</html>
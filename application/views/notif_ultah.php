<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Facebook</title>
        <link href="<?= base_url('/assets/normalize.css') ?>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <style >
     /* * {
        box-sizing: border-box;
    }
    .margin-icon {
        margin-left: 2px;
    }
    body {
        font-family: sans-serif;
        font-size: 0.9em;
        background: #e5e5e5;
    }
    .base-wrapper {
        background: white;
        box-shadow: 2px 1px 3px 0 rgba(0,0,0,.2);
        border-radius: 3px;
        overflow: hidden;
    }
    .main-container {
        width: 650px;
        display: block;
        margin: 0;
    }
    .flex-container {
        display: flex;
    }

    .flex-container.column {
        flex-direction: column;
    }
    .left-column {
        width: 350px;
    } */
    .navbar-wrapper {
        color: #888;
        background: white;
        padding: 10px;
        width: 1348px;
        margin: 0 0 10px 0;
        border-radius: 3px;
    }
    .ulangtahun-wrapper {
        width: 650px;
        margin-top: 17px;
        margin-left: 350px;
    }

    .ulangtahun-header {
        padding: 10px 15px;
        border-bottom: 1px solid #DDD;
        color: #888;
        font-weight: bold;
        text-align: left;
    }

    .ulangtahun-list {
        max-height: 230px;
        overflow: auto;
    }

    .ulangtahun-item {
        cursor: pointer;
        display: flex;
        padding: 10px 15px;
        border-bottom: 1px solid #DDD;
    }

    .ulangtahun-item:hover {
        background: #f6feff;
    }

    .ulangtahun-icon {
        width: 45px;
        height: 45px;
        background: #DDD;
        margin-right: 10px;
    }

    .ulangtahun-body {
        padding: 3px 0;
        flex: 1;
    }

    .ulangtahun-description {
        color: #2b2b2b;
        font-size: 1em;
        margin-bottom: 3px;
    }
    .ulangtahun-date {
        color: #888;
    }

    .pendidikan-wrapper {
        margin-top: 20px;
        padding: 15px;
    }

    .pendidikan-header {
        border-bottom: 1px solid #DDD;
        color: #888;
        font-weight: bold;
        padding-bottom: 7px;
        margin-bottom: 10px;
    }

    .pendidikan-list a {
        font-weight: bold;
        color: black;
        text-decoration: none;
        display: inline-block;
        margin-bottom: 5px;
    }

    .pendidikan-list a span {
        margin-right: 7px;
    }

    .pendidikan-list a.edit {
        color: #008eff;
        margin-left: 15px;
    }

    .pendidikan-list a:hover {
        text-decoration: underline;
    }

    .pendidikan-input {
        margin-bottom: 15px;
        margin-top: 10px;
    }

    .pendidikan-footer {
        border-top: 1px solid #DDD;
        padding-top: 10px;
        text-align: right;
    }

    .pendidikan-footer select {
        margin-right: 15px;
    }
    .pendidikan-footer button {
        padding: 5px 10px;
        border-radius: 3px;
        border: 0;
        outline: 0;
        margin-right: 3px;
        font-size: 0.8em;
    }
    .pendidikan-footer button.simpan {
        background: #008eff;
        color: white;
    }
    .pendidikan-footer button.batal {
        background: #DDD;
    }
    .ucapan-inner {
        margin: 7px;
        position: relative;
        width: 100%;
    }

    .ucapan-input {
        height: 75px;
        border: 1px solid #DDD;
        width: 100%;
        outline: 0;
        padding: 5px 10px;
        font-size: 1em;
    }

    .ucapan-actions {
        margin-top: 7px;
        text-align: right;
    }

    .ucapan-actions button {
        padding: 5px 15px;
        font-size: 1em;
        background: #008eff;
        color: white;
        border-radius: 3px;
        outline: 0;
        border: 0;
    }
    #menulist {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        height: 32px;
        border: 1px solid #000;
        width: 260px;
        text-indent: 8px;
    }

    .arrow {
        cursor: pointer;
        height: 32px;
        width: 24px;
        position: absolute;
        right: 0;
        background-color: #c8c8c8;
        background: url("http://icons.aniboom.com/Energy/Resources/userControls/TimeFrameDropDownFilter/Dropdown_Arrow.png") 0;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        text-align: left;
        padding: 14px 16px;
        text-decoration: none;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 5;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .navbar-item {
        float: left;

    }
    </style>
    </head>
<body>
<div id="app-notif-ultah">
    <div class="main-container">
        <div class="flex-container">
            <div class="flex-container column left-column">
                <div class="navbar-wrapper">
                    <div class="navbar-item">
                    <div class="dropdown">
                            <a href="">
                                <i class="fa fa-home fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navbar-item">
                    <div class="dropdown">
                            <a href="">
                                <i class="fa fa-group fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navbar-item">
                    <div class="dropdown">
                            <a href="">
                                <i class="fa fa-comment fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navbar-item">
                        <div class="dropdown">
                                <a href=""><i class="fa fa-bell fa-2x"></i></a>
                                <div class="dropdown-content" v-for="(notif, index) in notifUltahList">
                                    <div>{{notif}}</div>
                                    <div>hari ini berulang tahun</div>
                                </div>
                        </div>  
                    </div>
                </div>

                <!-- <div class="base-wrapper ulangtahun-wrapper">
                    <div class="ulangtahun-header">Now</div>
                    <div class="ulangtahun-list">
                        <div class="ulangtahun-item" v-for="(notif, index) in notifUltahList">
                            <div class="ulangtahun-icon"></div>
                            <div class="ulangtahun-body">
                                <div class="ulangtahun-description">{{ notif }}</div>
                                <div class="ulangtahun-date">Just Now</div>
                                <div class="ucapan-inner">
                                    <div class="ulangtahun-ucapan">
                                        <textarea class="ucapan-input" v-model="ucapanInput"></textarea>
                                        <div class="ucapan-actions">
                                            <button @click="adducapan">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="base-wrapper ulangtahun-wrapper"> <div
                class="ulangtahun-header">Recent</div> <div class="ulangtahun-list"> <div
                class="ulangtahun-item" v-for="notif in notifUltahList"> <div
                class="ulangtahun-icon"></div> <div class="ulangtahun-body"> <div
                class="ulangtahun-description">{{ notif }}</div> <div
                class="ulangtahun-date">Just Now</div> </div> </div> </div> </div> -->

                <!-- <div class="base-wrapper ulangtahun-wrapper"> <div
                class="ulangtahun-header">upcomming</div> <div class="ulangtahun-list"> <div
                class="ulangtahun-item" v-for="notif in notifUltahList"> <div
                class="ulangtahun-icon"></div> <div class="ulangtahun-body"> <div
                class="ulangtahun-description">{{ notif }}</div> <div
                class="ulangtahun-date">Just Now</div> </div> </div> </div> </div> -->
                <!-- <div class="base-wrapper pendidikan-wrapper">
                    <div class="pendidikan-header">PENDIDIKAN</div>
                    <div class="pendidikan-body">
                        <div class="pendidikan-list" v-if="!pendidikanActive">
                            <a href="#" v-for="(pendidikan, index) in pendidikanList">{{ pendidikan }}
                                <span v-if="index !== pendidikanList.length - 1">-</span>
                            </a>
                            <a href="#" class="edit" @click="setpendidikanActive(true)">Edit</a>
                        </div>
                        <div v-if="pendidikanActive">
                            <div class="pendidikan-input">
                                <input-tag :tags.sync="pendidikanTagsModel"></input-tag>
                            </div>
                            <div class="pendidikan-footer">
                                
                                <button class="simpan" @click="pendidikanSimpan">Simpan</button>
                                <button class="batal" @click="pendidikanBatal">Batal</button>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
    </div>
</div>
</div>    
<script src="<?= base_url('/assets/notif-ultah/assets/vue.js') ?>"></script>
    <script src="<?= base_url('/assets/notif-ultah/assets/vue-input-tag.min.js') ?>"></script>
    <script src="<?= base_url('/assets/notif-ultah/assets/axios.min.js') ?>"></script>
    <?php 
      echo '<script> var userId = ' . $_GET['user'] . ' </script>';
      if (isset($_GET['teman'])) {
        echo '<script> var temanId = ' . $_GET['teman'] . ' </script>';
      }
    ?>
    <script>
      Vue.component('input-tag', InputTag);

      new Vue({
        el: "#app-notif-ultah",
        data() {
          return {
            showModal: false,
            showModalError: false,
            hasCloseFriend: false,
            statusInput: "",
            notifications: [],
            pendidikanTagsModel: [],
            pendidikanList: [],
            pendidikanActive: false,
            notifUltahList:[],
          }
        },
        
        created() {
          this.getStatusTemanDekat()
          this.getPendidikan()
          this.getUltahTeman()
        },

        methods: {
            getUltahTeman(){
            axios.get('index.php/data/get', { 
              params: {
                 query: "select profil.nama_user, profil.tanggal_lahir_user from profil, teman where profil.id_user = teman.id_teman and day(profil.tanggal_lahir_user)=day(curdate()) and month(profil.tanggal_lahir_user)= month(curdate()) and profil.id_user !='" + userId + "';"
              }
            }).then((res) => {
              this.notifUltahList = res.data[0].nama_user.split(',')
            })
          },
          getPendidikan () {
            axios.get('index.php/data/get', { 
              params: {
                query: "select pendidikan_user from profil where id_user = '" + userId + "';"
              }
            }).then((res) => {
              this.pendidikanList = res.data[0].pendidikan_user.split(',')
            })
          },
          getStatusTemanDekat () {
            axios.get('index.php/data/get', { 
              params: {
                query: "select * from teman_dekat where id_user = '" + userId + "' and id_teman = '" + temanId + "';"
              }
            }).then((res) => {
              if (res.data.length) {
                this.hasCloseFriend = res.data[0].id
              } 
            })
          },
          updateTemanDekat () {
            let query;
            if (this.hasCloseFriend) {
              query = "DELETE FROM teman_dekat WHERE id = '" + this.hasCloseFriend + "';"
            } else {
              query = "INSERT INTO teman_dekat (id_user, id_teman) VALUES ('" + userId + "', '" + temanId + "');"
            }
            return axios.get('index.php/data/mutation', { 
              params: {
                query: query,
              }
            }).then((res) => {
              this.hasCloseFriend = !this.hasCloseFriend
              this.openModal()
            })
          },
          openModal() {
            this.showModal = true
          },
          closeModal() {
            this.showModal = false
          },
          openModalError() {
            this.showModalError = true
          },
          closeModalError() {
            this.showModalError = false
          },
          addStatus() {
            this.statusInput = ""
            if (this.hasCloseFriend) {
              this.notifications.push("Teman dekat anda menambahkan status baru")
            }
          },
          
          clearNotif(index) {
            this.notifications.splice(index, 1)
          },

          
          setpendidikanActive(state) {
                        if (state) {
                            this.pendidikanTagsModel = this.pendidikanlanList
                        }
                        this.pendidikanActive = state
                    },
                    updatependidikan() {
                        axios.get('index.php/data/mutation', {
                                params: {
                                    query: "UPDATE profil SET pendidikan_user = '" + this.pendidikanList.join(',') + "' where id_user = '" + userId + "';"
                                }
                            })
                            .then((res) => {
                                if (res.data) 
                                    alert('berhasil')
                            })
                    },
                    pendidikanSimpan() {
                        const tags = this.pendidikanTagsModel
                        this.pendidikanList = tags
                        this.pendidikanTagsModel = []
                        this.setpendidikanActive(false)
                        this.updatependidikan()
                    },
                    pendidikanBatal() {
                        this.pendidikanTagsModel = []
                        this.setpendidikanActive(false)
                    }
                }
            })
    </script>            
</body>
</html>
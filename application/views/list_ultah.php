<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Facebook</title>
        <link href="<?= base_url('/assets/normalize.css') ?>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <style >
     /* * {
        box-sizing: border-box;
    }
    .margin-icon {
        margin-left: 2px;
    }
    body {
        font-family: sans-serif;
        font-size: 0.9em;
        background: #e5e5e5;
    }
    .base-wrapper {
        background: white;
        box-shadow: 2px 1px 3px 0 rgba(0,0,0,.2);
        border-radius: 3px;
        overflow: hidden;
    }
    .main-container {
        width: 650px;
        display: block;
        margin: 0;
    }
    .flex-container {
        display: flex;
    }

    .flex-container.column {
        flex-direction: column;
    }
    .left-column {
        width: 350px;
    } */
    .navbar-wrapper {
        color: #888;
        background: white;
        padding: 10px;
        width: 1348px;
        margin: 0 0 10px 0;
        border-radius: 3px;
    }
    .ulangtahun-wrapper {
        width: 390px;
        margin-top: 2px;
        margin-left: 10px;
    }

    .ulangtahun-header {
        padding: 10px 15px;
        border-bottom: 1px solid #DDD;
        color: #888;
        font-weight: bold;
        text-align: left;
    }

    .ulangtahun-list {
        max-height: 230px;
        overflow: auto;
    }

    .ulangtahun-item {
        cursor: pointer;
        display: flex;
        padding: 10px 15px;
        border-bottom: 1px solid #DDD;
    }

    .ulangtahun-item:hover {
        background: #f6feff;
    }

    .ulangtahun-icon {
        width: 45px;
        height: 45px;
        background: #DDD;
        margin-right: 10px;
        
    }

    .ulangtahun-body {
        padding: 3px 0;
        flex: 1;
    }

    .ulangtahun-description {
        color: #2b2b2b;
        font-size: 1em;
        margin-bottom: 3px;
    }
    .ulangtahun-date {
        color: #888;
    }

    .pendidikan-wrapper {
        margin-top: 20px;
        padding: 15px;
    }

    .pendidikan-header {
        border-bottom: 1px solid #DDD;
        color: #888;
        font-weight: bold;
        padding-bottom: 7px;
        margin-bottom: 10px;
    }

    .pendidikan-list a {
        font-weight: bold;
        color: black;
        text-decoration: none;
        display: inline-block;
        margin-bottom: 5px;
    }

    .pendidikan-list a span {
        margin-right: 7px;
    }

    .pendidikan-list a.edit {
        color: #008eff;
        margin-left: 15px;
    }

    .pendidikan-list a:hover {
        text-decoration: underline;
    }

    .pendidikan-input {
        margin-bottom: 15px;
        margin-top: 10px;
    }

    .pendidikan-footer {
        border-top: 1px solid #DDD;
        padding-top: 10px;
        text-align: right;
    }

    .pendidikan-footer select {
        margin-right: 15px;
    }
    .pendidikan-footer button {
        padding: 5px 10px;
        border-radius: 3px;
        border: 0;
        outline: 0;
        margin-right: 3px;
        font-size: 0.8em;
    }
    .pendidikan-footer button.simpan {
        background: #008eff;
        color: white;
    }
    .pendidikan-footer button.batal {
        background: #DDD;
    }
    .ucapan-inner {
        margin: 7px;
        position: relative;
        width: 100%;
    }

    .ucapan-input {
        height: 75px;
        border: 1px solid #DDD;
        width: 100%;
        outline: 0;
        padding: 5px 10px;
        font-size: 1em;
    }

    .ucapan-actions {
        margin-top: 7px;
        text-align: right;
    }

    .ucapan-actions button {
        padding: 5px 15px;
        font-size: 1em;
        background: #008eff;
        color: white;
        border-radius: 3px;
        outline: 0;
        border: 0;
    }
    #menulist {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        height: 32px;
        border: 1px solid #000;
        width: 260px;
        text-indent: 8px;
    }

    .arrow {
        cursor: pointer;
        height: 32px;
        width: 24px;
        position: absolute;
        right: 0;
        background-color: #c8c8c8;
        background: url("http://icons.aniboom.com/Energy/Resources/userControls/TimeFrameDropDownFilter/Dropdown_Arrow.png") 0;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        text-align: left;
        padding: 14px 16px;
        text-decoration: none;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 5;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .navbar-item {
        float: left;

    }
    </style>
    </head>
<body>
<div id="app">
    <div class="main-container">
        <div class="flex-container">
            <div class="flex-container column left-column">
                <!-- <div class="navbar-wrapper">
                    <div class="navbar-item">
                    <div class="dropdown">
                            <a href="">
                                <i class="fa fa-home fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navbar-item">
                    <div class="dropdown">
                            <a href="">
                                <i class="fa fa-group fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navbar-item">
                    <div class="dropdown">
                            <a href="">
                                <i class="fa fa-comment fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navbar-item">
                        <div class="dropdown">
                                <a href=""><i class="fa fa-bell fa-2x"></i></a>
                                <div class="dropdown-content" v-for="(notif, index) in notifUltahList">
                                    <div>{{notif}}</div>
                                    <div>hari ini berulang tahun</div>
                                </div>
                        </div>  
                    </div>
                </div> -->

                <div class="base-wrapper ulangtahun-wrapper">
                    <div class="ulangtahun-header">Now</div>
                    <div class="ulangtahun-list">
                        <div class="ulangtahun-item" v-for="(notif, index) in notifUltahList">
                            <div class="ulangtahun-icon"><img style="width:45px; height:45px; border-radius: 50px 50px;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhIVFhUVGBcVFRgVGBUVFRUVFRgWFxcVFxUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGBAQGi0dHx0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSstLSsrLS0tNy03NystNysrKysrK//AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAABAUGCAIDBwH/xABEEAABAwICBQgHBQYGAwEAAAABAAIDBBEFIQYSMUFRByI1YXFzgbEIEzKRobLBI0JSctEUJDM0gvBidJKis+FjwvFD/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAIxEBAQACAQUAAwADAAAAAAAAAAECEQMEEiExQSIyQhMjUf/aAAwDAQACEQMRAD8A4ahCEAhCEHTPR86VPcS+bFZVVq9HzpU9xL5sVlUFS8S/iS94/wCcrS0pTio+1k7x/wAxSMlWiHj3LWXLx7llAy+Z/wDqlDZTwEkdfwSp0urzGbd7lqqJtX7NvtH2jw6lsp4g0BBsiit1neSswvd6LIjb1Y3Qc16pHmtbaUzV9UScinKqY52QCSsw128LO5RpMalWgmlAb9jMeacmngT9F0LV4bCuIupHNNwNi6LoZjJkZ6p/tN9kneOvsVNp0lRZ1LxsN9q2Qu1hbes2lEI1pFo56weti5srcxwePwuUNhj1S5wbkbtmjO6+3LguvX6upRDTPCi395hbzm5SDc9vWOKtjUVzmek9TMW/dPOaeLTsT5TyZArZidK2alE0f/5m9t4afab4HNIaSTKyUSOmxCw2IOKptpWE7ljUQO4LLTpn6nejqtZyWVL9iZ8FYbp3qW7FTN1dNNkNRtC7no5/KU/dR/IFw6Zq7jo7/K0/dR/IFrx+nN1eOuQ18pXRVb3Enkqhq3nKV0VW9xJ5Koa0coQhCAQhCAQhCAQhCDpno+dKnuJfNisqq1ej50qe4l82KyqCpeKH7R/eSfO5InOSrFT9q/8APJ8xSJzleIeNbc24pxis0OfuZkOty0Qt1W6xHOOQ8VniTdVrIxvzKIY4fCXHWdtcbpb1/wB2WUY1WHsssYxtUorK+S9cPevGi5WVlCGNrJzwzCzIQbZFY4PQGaQNA5o29a6Zh2ENY0WCw5uTXiOrg4u7zUbpdHRwW92Aj8Kl0dOtjqZcfdXb2RBJcBbwTY6gMDw9o2LpEtAm+uwy4zCvMqplhLCOiqg4Ne3YR/YTha/ao5RfYymN3sON29TurtT9GSF043ujizx7borbtWU8Ou0tP3gR71rY/wBxW5hUquVQH9krHwv/AIUuswg7Ln9V4yja19t1078ptAC5kgyLh/ubmPgo0ao+rbLfP2XfmCtfPlCe4Lh7C1bsQwTgLqOaOaRtyBNip/Q1bZAM1SxaZWIlRYdqkrdWxKWTYeDmAmmsoiNoyWeU27en5scfFRmVmS7Xo3/KU/dR/KFyiWj4LrWAC1NAP/Ez5QrcX063VksNPKV0VW9xJ5Koat5yldFVvcSeSqGtnAEIQgEIU50G0dp6iBz5mFzhIWghzm5BrDawPElBBkIQgEIQg6Z6PnSp7iXzYrKqtXo+dKnuJfNisqgqPjOUr/zv+cpNSQ67rbt624077eT87/ncnDD4BHGXHaVeK1q1daZrBsbn7lqqjrTjgPotmDuu6V53Cw81qhzffiPMqUfSqQ7AgHLtWLz9oR2eS9aiGYbbavXA7AMzkPFbafMpdglL6yYcAVXO9s2thj3XScaHYYI4wbZqVsskVDDqtAS4NXn2216eOpGbGrcGLU1bWlFtvS1J547pSvHBQlDtIaHWabbRmEkwmuL2Z+03J3apRikFwoQ37GoufZfkeF9y04svOmHPhubSeDYt7XHJJWH9fBbw9dFccMWnUQdAD+BzT4E2PwKhlDhhfRVFtrLPH9O34Ka6XH92k/p+DgmvQexjlBGR1gew5K0/VH1y5kxBvdSTA9KZIiLm4UdxOnMcr2H7rnN9xK0tKlOnZaLTmMtFzmpDh2KicbFwCKWxU10X0tMVmu2KtiHUKnCb5t2qb4Uy0MYO0MaD7goRhGORTDJwBU7o/wCGz8o8kxiblb4MHKV0VW9xJ5Koat5yldFVvcSeSqGrICEIQC6fyYfyr++d8ka5gul8mcrRSvuQPtnfJGg5ohCEAhCEHTPR86VPcS+bFZVVq9HzpU9xL5sVlUFSBD6ypk4CSS/g91kqxqcNaGhL6amEZlcdpkkP+9xUexGQuetJPCnunLA47QPO83SVj+cOz6p0wplobJpcPtrdX1Q+lE+Urv6fJZ8F5Wj7U9bW/VZCxP8Ae1AojNhlv2dXX4fRTPQ6gHtkbc/BQaoecmsGZy7B976DxKe6HFKiIAAC3XdY8vnw14r2+XWIQlAXMo9LKkbQPcn7C9InP9rxXNcdOrHOVMrr0Jqp626WetyWdraFoeF560JpqKmyj+JYjJ9y975lJ5LdJhM9p2lQTTIsa02IvtHamiplqHnORw6rpPPhErxzpfiSfir44yXe2WeWV9RJ8CxISwtO+1j4J1DrBQnRdhp5XRvdrB3Ob28FMWuuBntz8P7uune447NU06Wu/dn8CWfMEz8n8txM3gXfMU96UC8Dh1sPue0qMaFkg1B4hx+JVpPCPqPcoNNqV0vB2q8f1NBPxuo61S7lNb+8xu/FCw+I1goiEiazC2NdZawsgpQdcNxiSI3a4q0Wi8xfR0zztdDE49pY0qpBVsdC+j6P/Lw/8bUCPlK6Kre4k8lUNW85Suiq3uJPJVDQCEIQCdMLxF8bC1psCb/AD6JrW+DZ4oNCEIQCEIQdM9HzpU9xL5sVlVWr0fOlT3EvmxWVQVm0kmDC4AZ67/MqOQRFzvinbSJ15X/nf8xXlDTZXWrMromfZW60wu/inqNlIaJ1mkcLqOwG8knahC7Ef4g62N8yvIQLn3+5e122M/4fIj9VjTn9fdmPooTD7gbG62s4jLIbhff8bqSOxWC+qXC+zcPNQiKGQhrGbXOtc+yL8U8Y/ooyCOGTnS31hK4k21iAWDLY3auXs77vbq7+2ekiZHBJmwgnqcCR4LOOma3MZqEaO0DpZo42wsjJc0XY5xOqDd8jnE5DVCns8Qjl1A4vj2a20t6id4WfJx3H614+SZeLCygfsT7C24TA2LUfkclJ8MddqxreGuvZYJnmta7lJ8Qp7lR+vpRrtGqTGPattPV2KuE2tldRGMQ0ohgOqM3fhYLnquVqj0kdPcNgmsGF3s3JsbE222BsFv00wl2uZaVxaH6nsAazHMBaBbhbglugWEyxkzzuOTHsbrnnvdI4Oc4jcMvj1Ls/x4SOK8vJfiJU+MXnjP8Aisew5ZhdCpj8VC8fwTXndIwatjc23uve6kmEu1oxbs8d6nGzXhnnL7rdjf8ADcOIPlf6KO6Exg+sJ4O+JT5j7uYO0/K5M2heULjxIHvK1npn9IeUzD5XyxOYxzmiIAkC4HOOSgb4XNObSO0KxkNWxoDHAbBkVrrMGpJxzo23O8ZKsqVdwsguq47yZNN3U7vBc8xXBJoHESMITaDerX6F9H0f+Xh/42qp6thoX0fR/wCXh/42qYEfKV0VW9xJ5Koat5yldFVvcSeSqGpAhCEAt8GxaFINH8BM8ZeHAWcW+4NP1QR9CEIBCEIOmej50qe4l82KyqrV6PnSp7iXzYrKoKtYhGXTP4eseP8AcU500Fm2Xkkd5JOp7/mKVN2WW0Y2m6nNjJ7/AIKO4aSSTx1lIKrmukufuElR/C9/Vn4b1C0Lak3aw9oSzB6fWfbw96SSjm24OBHin7RCDWk+J8f+rLLlusWvFjvKRKqTCBqjLrTj+zHV1fu7CNoPaniiphZb3U4XBOR6HZEbp6FsY5jAOwWShsJPte7cnZ0C1SRqe7adSGqYZp/wQ5Jll2p4wd2QCrVoc6iO5TZXUN8wnuYbFqNis5dVMu4ixpXXWbaXqT/JTA7lgKeytcko7U4bketMOC3a+SM/ddrDsP8A2CugS04IsoHijPVVXAPb8Wn/ALWvDn+WnP1GH47INLqnVjHWXfI4fVbNFIbQRjiQe2yZtOZ7uYwbCPmIH0KlWAw2EY3NbcjrOzyXb/LhntHdIccIqZADk06o8AAvKbSRw3lM2lNKWVD+DiXDxTaxySIrolFpW4bSnKWvgqW6sgBvv3rm0Dyl0ErhsKdpsn0r0VMJL482HNWF0L6Po/8ALw/8bVxePEiWFj8wcs12/RpoFJTgbBDEB/oCiJNnKV0VW9xJ5Koat5yldFVvcSeSqGpAhCEApHo5iBjiIB++T8G/oo4nDDzzT2/QIG9CEIBCEIOmej50qe4l82KyqrV6PnSp7iXzYrKlBX2WIBz/AM7r/wCorSAcgAldVHeR9/xO8ykGMVwhZl7Ryb2rZz/TJpNOGtc0G732Dupo/VNlDzS0nY8ap8Vrc1ziS7MnMrbEzWhPFqq19QoDuaRvGR/p2fBS7k+AL3dVvIKHB9yH7pB/uClXJ7NaaVu/I+8BYdR+rbp7+bqtKQlRCbad6XMkXnvSseOjSWqFglzimvECbFXitNEkl3ZJ6wobEzQQ2F08YfXAbAEyTj4SMi7U2VMxY4X2HzXkmLtbtKMQla+PKxNwR71lVsZYVxPBW0hNlE4paHKaXF7IFBtNmWlgdbaS33hTcuUP5QG82A8JW/EFTxX84ryT/XXPcavNXNjH3dUHwF//AG+C6Bh0ftkdTR2NAHndQXRoesqZ592s4N7ASLqf0w1Wht8xb3lell6eXEX08obtZIN2RUJa1dbxai9ZC9p22uFyt0diRwyU43wjLwyhTlT5puYltM6ysps6tp8l3TRsWpKcf+GP5AuJ0Zuu34D/AC0HdM+UKq0NHKV0VW9xJ5Koat5yldFVvcSeSqGiwQhCAT7gWGukjLhbJxHwafqmJTLQ3+C7vD8rEENQhCAQhCDpno+dKnuJfNisqq1ej50qe4l82KyqDgOMVTYi9zj953mVCf2szz3dsGwJ4xON0kjy47HvsNw5xTVNDqPDgtWcmmVdBqOB3LHDraz28b2Tu9okZuTG1pZIL9iDClZdr497SS1PuhNTaqYdz2lviM/1TJINSQnrv4JdRu9VOyQezrB/Z+Ie4rPkx3jYvx5ayldngSppSGjku0EJYF5j1t+CoOWmoiuLID1jLOFMQZsQw0kW9Y4D/DlfxXtJh2Q1XEW8VvqZCVuoZGEEB41twSpk28fgrJAA+7u1PNPQsY0BoSamq44/bdc9W5LmTMeLscCs6vdxrEVlk4LS2pF7FKA66ijEKGcpk4ZFCTukLv8ASx5/RTNy55yku9bNBTtOYBe/qDzYfBj/AHhX4JvOM+a646b9DqHVijG93OP99vkpZIyzvckuBUurnwFm9g2fUpTOeeV6GXt5k9FIdlmua6WUnqqhwGx3OHiujjcohyjQWMb+ohTh7MkQYlcJzSKMpXEtWR8w5+xd1wL+Wh7tnyhcConLvmAH92g7qP5Qoq2Jp5Suiq3uJPJVDVvOUroqt7iTyVQ1VYIQhAKZ6GRkwO7w/KxQxTTQuS0Du8PysQQtCEIBCEIOmej50qe4l82KyqrV6PnSp7iXzYrKoK4YhDqyPv8Ajd5lIayn1m3CkOKwc59s+c7zKamsyIK3npjsgw59hYrLEKXWF7da0REh5G66d4RcKCoxVPzaT2Fb6N4IMbu1p4JRjtBdpLN3xsmumfrAHeFC0dQ0JxPWj9U486PLtbuKlzHLjuG4gWPbI084eAeN7Suk4VibZWhwO34HgVwc/F23cehwcvdNX2fHjJM1Y2TWuH2HCychJksXWIWEdCPVMcjjYyX8LLQzD5b5OOSdqiDO9kRTavFX3Hdw8vHJ5huGGSHa8jil9JhRHsyvB6jklDalp239yWUoJOQsFXLKf8b59RhrUa4cNew39Y53anaCSwzXmuLLTIcljfLhvms6+uZGx0kjgGsBc4ncBmVzSiqH1Erql4IMpJaDtbGLAD3AJw0jxUVDvVNzhY7ncJHjYPytt4nsSD9r1Dlm47AN36Bd3TcXbO6/XndVzTK9s9RL6JwAA4haJJcyd6acGe8v1nHYMh/fild7uJ3bltZ5c0pzpiTZMnKPH9g08HJ8ws3TRp+68DuqxTH2VzeNyWRFN8RSyFy1ZHSjOasBo9/Kwd1H8oVe6VysHo5/KU/dR/IFGScTXyldFVvcSeSqGrecpXRVb3Enkqhqq4QhCAUq0VktE785+Vqiqkujb7RH858moI0hCEAhCEHTPR86VPcS+bFZVVq9HzpU9xL5sVlUHEaxvOd+Z3mUzTsANhvW6orzrv8AzOHZzik0kgPVxK3jCmrEG2dcJbQvuBZeVUXNWrC3Z/BE/DjLACFFsSofVyaw9lxzHA8VMo2XuXeC0VVGHN52YIUEqKwvF7EZH4FO2HYnJTPvtbv6+3r601S0xa4sPgeI3Fb6Kf7j/AqtxlmqvLZdx03CMYjmaC0+CdoTcrkBp5Yna8LrHhuUs0c0wDiGTj1cmzP2XdhXFy8Fx8x3cXPMvFT8U11kMOBWmkrQQLFOENYFz706Z5aWYS1KBSALZ+1BJazEGsaXOcABvKrld+l5KJrDNQHSbST1rjDCSGDJ7xtdxYw8OLvALRpNpWZQY4narTk529w4NHDrUYY69msv9T+i6uDg/rJydR1P84lplAsGjZl1D/tL8OprgudkNt+PisMPwvK8mQ2gbyeCylmdNL6hg1I4z9oeNt3Yuz04faQ4QzWBcBZuwdfWsZLg23cVvwiS97eyMh2JU+EOPFZfV/jzBNq06RUXrWyMvu+KdKSACxAsttXTG9+ITY4tW4c+F1nDLcV5E9dIxOgBBD23GxQ/FtH3R85mbeG9aS7UsJ6ZysPo3/KU/cx/IFW+nfY5qx+jB/c6buYvkCZIxNvKV0VW9xJ5Koat5yldFVvcSeSqGqrhCEIBPmBy2jP5j5NTGnfCDzD+Y+QQNCEIQCEIQdM9HzpU9xL5sVlUIQVpqZS2SQOH33fMVnHKNoF+1CFtiyrcXAg8Uip2Webf2UIUqnyN9mW3nIdZSpkWVuKEKSGbGqAubcDnNzH6Jhkj12gjIjzQhVXLMOq9YajvaHFaaljrm9iOBXqEQU4djcsRGq8gcDmFIaXS+Ue0xruw2QhZXiwy9xrjy54+q3y6WVDhZkbATxds8LJmrYquoP2swA3AbAhCjHiwx9ROXNnl7rGDR9gPPkJy42S6F0UZtE253naPevEKykYYbUumqWgm4Zm47stwSyZoYXNAu6Rxc88ATk1CFH1Y8YXGQLBOYOqhCj6FNJNrFOoAtdCFXKJhO6APysmuuw3aBu3dSEKNpRbFcAa+5aNV/wACuxaNRltJTtO0Qxg9oYEIV9q6NnKV0VW9xJ5KoaEIBCEIBSTRsN9Ub/jPk1CEH//Z" alt=""></div>
                            <div class="ulangtahun-body">
                                <div class="ulangtahun-description">{{ notif }}</div>
                                <div class="ulangtahun-date">Just Now</div>
                                <div class="ucapan-inner">
                                    <div class="ulangtahun-ucapan">
                                        <textarea class="ucapan-input" v-model="ucapanInput"></textarea>
                                        <div class="ucapan-actions">
                                            <button @click="adducapan">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="base-wrapper ulangtahun-wrapper"> <div
                class="ulangtahun-header">Recent</div> <div class="ulangtahun-list"> <div
                class="ulangtahun-item" v-for="notif in notifUltahList"> <div
                class="ulangtahun-icon"></div> <div class="ulangtahun-body"> <div
                class="ulangtahun-description">{{ notif }}</div> <div
                class="ulangtahun-date">Just Now</div> </div> </div> </div> </div> -->

                <!-- <div class="base-wrapper ulangtahun-wrapper"> <div
                class="ulangtahun-header">upcomming</div> <div class="ulangtahun-list"> <div
                class="ulangtahun-item" v-for="notif in notifUltahList"> <div
                class="ulangtahun-icon"></div> <div class="ulangtahun-body"> <div
                class="ulangtahun-description">{{ notif }}</div> <div
                class="ulangtahun-date">Just Now</div> </div> </div> </div> </div> -->
                <!-- <div class="base-wrapper pendidikan-wrapper">
                    <div class="pendidikan-header">PENDIDIKAN</div>
                    <div class="pendidikan-body">
                        <div class="pendidikan-list" v-if="!pendidikanActive">
                            <a href="#" v-for="(pendidikan, index) in pendidikanList">{{ pendidikan }}
                                <span v-if="index !== pendidikanList.length - 1">-</span>
                            </a>
                            <a href="#" class="edit" @click="setpendidikanActive(true)">Edit</a>
                        </div>
                        <div v-if="pendidikanActive">
                            <div class="pendidikan-input">
                                <input-tag :tags.sync="pendidikanTagsModel"></input-tag>
                            </div>
                            <div class="pendidikan-footer">
                                
                                <button class="simpan" @click="pendidikanSimpan">Simpan</button>
                                <button class="batal" @click="pendidikanBatal">Batal</button>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
    </div>
</div>
</div>    
<script src="<?= base_url('/assets/notif-ultah/assets/vue.js') ?>"></script>
    <script src="<?= base_url('/assets/notif-ultah/assets/vue-input-tag.min.js') ?>"></script>
    <script src="<?= base_url('/assets/notif-ultah/assets/axios.min.js') ?>"></script>
    <?php 
      echo '<script> var userId = ' . $_GET['user'] . ' </script>';
      if (isset($_GET['teman'])) {
        echo '<script> var temanId = ' . $_GET['teman'] . ' </script>';
      }
    ?>
    <script>
      Vue.component('input-tag', InputTag);

      new Vue({
        el: "#app",
        data() {
          return {
            showModal: false,
            showModalError: false,
            hasCloseFriend: false,
            statusInput: "",
            notifications: [],
            pendidikanTagsModel: [],
            pendidikanList: [],
            pendidikanActive: false,
            notifUltahList:[],
            

          }
        },
        
        created() {
          this.getStatusTemanDekat()
          this.getPendidikan()
          this.getUltahTeman()
        },

        methods: {
            getUltahTeman(){
            axios.get('index.php/data/get', { 
              params: {
                 query: "select profil.nama_user, profil.tanggal_lahir_user from profil, teman where profil.id_user = teman.id_teman and day(profil.tanggal_lahir_user)=day(curdate()) and month(profil.tanggal_lahir_user)= month(curdate()) and profil.id_user !='" + userId + "';"
              }
            }).then((res) => {
                
              this.notifUltahList = res.data[0].nama_user.split(',')
            })
          },
          getPendidikan () {
            axios.get('index.php/data/get', { 
              params: {
                query: "select pendidikan_user from profil where id_user = '" + userId + "';"
              }
            }).then((res) => {
              this.pendidikanList = res.data[0].pendidikan_user.split(',')
            })
          },
          getStatusTemanDekat () {
            axios.get('index.php/data/get', { 
              params: {
                query: "select * from teman_dekat where id_user = '" + userId + "' and id_teman = '" + temanId + "';"
              }
            }).then((res) => {
              if (res.data.length) {
                this.hasCloseFriend = res.data[0].id
              } 
            })
          },
          updateTemanDekat () {
            let query;
            if (this.hasCloseFriend) {
              query = "DELETE FROM teman_dekat WHERE id = '" + this.hasCloseFriend + "';"
            } else {
              query = "INSERT INTO teman_dekat (id_user, id_teman) VALUES ('" + userId + "', '" + temanId + "');"
            }
            return axios.get('index.php/data/mutation', { 
              params: {
                query: query,
              }
            }).then((res) => {
              this.hasCloseFriend = !this.hasCloseFriend
              this.openModal()
            })
          },
          openModal() {
            this.showModal = true
          },
          closeModal() {
            this.showModal = false
          },
          openModalError() {
            this.showModalError = true
          },
          closeModalError() {
            this.showModalError = false
          },
          addStatus() {
            this.statusInput = ""
            if (this.hasCloseFriend) {
              this.notifications.push("Teman dekat anda menambahkan status baru")
            }
          },
          
          clearNotif(index) {
            this.notifications.splice(index, 1)
          },

          
          setpendidikanActive(state) {
                        if (state) {
                            this.pendidikanTagsModel = this.pendidikanlanList
                        }
                        this.pendidikanActive = state
                    },
                    updatependidikan() {
                        axios.get('index.php/data/mutation', {
                                params: {
                                    query: "UPDATE profil SET pendidikan_user = '" + this.pendidikanList.join(',') + "' where id_user = '" + userId + "';"
                                }
                            })
                            .then((res) => {
                                if (res.data) 
                                    alert('berhasil')
                            })
                    },
                    pendidikanSimpan() {
                        const tags = this.pendidikanTagsModel
                        this.pendidikanList = tags
                        this.pendidikanTagsModel = []
                        this.setpendidikanActive(false)
                        this.updatependidikan()
                    },
                    pendidikanBatal() {
                        this.pendidikanTagsModel = []
                        this.setpendidikanActive(false)
                    }
                }
            })
    </script>            
</body>
</html>
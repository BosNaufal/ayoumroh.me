
function easyREST(url, options) {
  return new Promise((resolve, reject) => {
    var type = options.type
    if (!type) reject("no_types")
    
    var request = options.request || {}
    request.type = type

    var query = options.query

    var file = options.file
    if (file && Object.keys(file).length) {
      var formData = new FormData()
      
      function appendToFormData(obj) {
        var objKey = Object.keys(obj)
        for (var i = 0; i < objKey.length; i++) {
          var key = objKey[i]
          var value = obj[key]
          console.log(key, value)
          formData.append(key, value)
        }
      }
      appendToFormData(request)
      appendToFormData(file)
      request = formData
    }
    axios.post(url, request, { params: query })
      .then(resolve)
      .catch(reject)
  }) 
}

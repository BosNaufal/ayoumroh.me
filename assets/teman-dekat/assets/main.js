var stringku = "halo";
var numberku = 10;     
var booleanku = false;
var objekku = {
  nama: "Ainur",
  nim: "h123123213",
  umur: 12,
}

function greeting (nama) {
  console.log("Halo " + nama)
}

var func2 = function () {
  console.log("Ini juga function lho...")
}

greeting(objekku.nama)
func2()

console.log(objekku.nama, objekku.nim)

var objekBaru = {
  nama: "Bayu",
  umur: 1,
  sapa: greeting,
  
  saying (kata) {
    console.log("AKU BERKATA " + kata)  
  },

  teman: objekku,
}

objekBaru.sapa(objekku.nama)
objekBaru.saying("AKU BELAJAR BARENG!")

console.log(objekBaru)

objekBaru.nama = "Bayu Utomo"

console.log(objekBaru)

var arrayku = [
  "ainur",
  "bayu",
  "bagus",
  "yudha",
]

console.log(arrayku)
arrayku.push("Ulum")
console.log(arrayku)

console.log(arrayku[1])
var fieldYangDiambil = "nama"
console.log(objekBaru["nama"])
console.log(objekBaru[fieldYangDiambil])
console.log(objekBaru.nama)
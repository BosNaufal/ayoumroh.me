Vue.component('request-button', {

  props: ["onDone"],

  template: `
    <button @click="requestData">
      {{ loading === true? 'Loading...' : 'Request Data' }}
    </button>
  `,

  data: function () {
    return {
      loading: false,
    }
  },

  methods: {
    requestData() {
      var me = this
      this.loading = true
      axios.get('https://swapi.co/api/people/?format=json')
        .then(function (response) {
          me.loading = false
          me.onDone(response.data.results)
        })
    },
  }
})